export type GenericKey = string | number | symbol

export class SpecialMap<K extends keyof Record<GenericKey, any>, V = any> extends Map<K, V>
{
  constructor(entries?: readonly (readonly [K, V])[] | null)
  {
    super(entries)
  }

  public toObject(): Record<GenericKey, any>
  {
    let obj: Record<GenericKey, any> = {}
    this.forEach(<K extends keyof Record<GenericKey, any>>(v: V, k: K) => obj[k] = v)
    return obj
  }

  public valuesAsArray(): V[]
  {
    let result: V[] = []
    this.forEach((value: V) => result.push(value))

    return result
  }

  public keysAsArray(): K[]
  {
    let result: K[] = []
    this.forEach((value: V, key: K) => result.push(key))

    return result
  }
}