export interface InifinityObject
{
  [key: string]: any
}

export interface IStaticClass
{
  new(...args: any[]): any
}

export interface Serializable
{
  toJSON: () => object
  toString: () => string
}

export interface SimpleKeyNumberObject<V>
{
  [key: number]: V
}

export interface SimpleKeyStringObject<V>
{
  [key: string]: V
}

export type UnknownObject = InifinityObject

export type StaticImplements<I extends new (...args: any[]) => any, C extends I> = InstanceType<I>;

export type StaticClass<T> = { [K in keyof T]: T[K] }

export type NonConstructorKeys<T> = ({[P in keyof T]: T[P] extends new () => any ? never : P })[keyof T]
export type NonConstructor<T> = Pick<T, NonConstructorKeys<T>>

export type ArrayFilterCallback<V> = (value: V, index: number, array: any[]) => boolean
export type ArraySortCallback<V> = (a: V, b: V) => number

export const Delay = (ms: number): Promise<void> => new Promise((resolve, reject) => setTimeout(() => resolve(), ms))

/**
 * My famous (not really) isEmpty function. It checks if a given variable is empty or not
 * @param {*} variable Anything you want to know if it's empty or not
 * @returns {Boolean} Returns true on empty and false on not empty
 */
export function isEmpty(variable: any): boolean
{
  if (variable === null || variable === undefined || Number.isNaN(variable)) return true
  if (typeof variable === "string" && variable === "") return true
  else if (typeof variable === "object") {
    if (Array.isArray(variable)) {
      if (variable.length === 0) return true
    } else if (Object.keys(variable).length === 0) return true
  }
  return false
}

export function randomIntRange(min: number, max: number): number {
  return Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + Math.ceil(min);
}

export function randomChance(chancePercent: number = 50): boolean
{
  let chance = randomIntRange(0, 100)
  return (chance >= chancePercent)
}

export function randomStringGenerator(length: number, includeNumbers: boolean = true): string
{
  let result = '';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  if (includeNumbers === true) characters += "0123456789"
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) result += characters.charAt(Math.floor(Math.random() * charactersLength));
  return result;
}

export function tryConvertToJSON(input: string): UnknownObject | string
{
  try {
    return JSON.parse(input)
  } catch (e) {
    return input
  }
}

export function autoConvertString(input?: string, defaultReturnValue: any = undefined): any
{
  if (typeof input !== "string") return defaultReturnValue
  let numb = parseInt(input)
  if (!Number.isNaN(numb)) return numb
  let obj = tryConvertToJSON(input)
  if (typeof obj === "object") return obj
  if (input === "true") return true
  if (input === "false") return false
  if (input === "null") return null
  if (input === "undefined") return undefined
  return input
}

export function checkBothBools(requiredTrue: any, optionalTrue: any): number
{
  if (!requiredTrue) return 1
  if (requiredTrue !== optionalTrue) return 2
  return 0
}

export function isServiceStatic(service: any): boolean
{
  return (!! service.prototype && !! service.prototype.constructor)
}

export function isDeepInstanceOf(type: any, type2: any): boolean
{
  if (! isServiceStatic(type) && isServiceStatic(type2)) type = type.constructor
  else if (isServiceStatic(type) && ! isServiceStatic(type2)) type2 = type2.constructor
  let proto: any = Object.getPrototypeOf(type)
  if (type === type2) return true
  while (proto !== type2 && proto !== null) {
    proto = Object.getPrototypeOf(proto)
  }
  return (proto !== null)
}

export function gitMergeArray(origin: any[], target: any[]): any[]
{
  for (let i = 0; i < origin.length; i++) {
    const value = origin[i];
    if (target.indexOf(value) === -1) origin.splice(i, 1)
  }
  
  for (let i = 0; i < target.length; i++) {
    const value = target[i];
    if (origin.indexOf(value) === -1) origin.push(value)
  }

  return origin
}

export function isClass(potentionalClass: object): boolean
{
  try {
    return (potentionalClass.constructor.name === 'Function')
  } catch (e) {
    return false
  }
}
